﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using WebMvcMocking.NET.Command;
using WebMvcMocking.NET.Models;
using WebMvcMocking.NET.Persistence;

namespace WebMvcMocking.NET.Tests.Command
{
    [TestClass]
    public class CreateUserCommandTest
    {
        [TestMethod]
        public void TestExecute()
        {
            // создаем объект, который контроллер передаст команде
            var userModel = new UserModel {Name = "Ivanov", Email = "ivanov@test.com"};
            const int expectedUserId = 2;
            var userRepositoryMock = new Mock<IUserRepository>();
            userRepositoryMock.Setup(r => r.Create(It.Is<User>(
                    // проверка свойств переданного в мок аргумента
                    u => u.Id.Equals(0) && u.Name.Equals(userModel.Name) && u.Email.Equals(userModel.Email) && u.CreateDate != null
                )))
                .Returns(expectedUserId)
                .Verifiable(); // этот метод позволяет в дальнейшем проверить вызов мока
            var command = new CreateUserCommand(userRepositoryMock.Object);
            var actualUserId = command.Execute(userModel);
            Assert.AreEqual(expectedUserId, actualUserId);
            userRepositoryMock.Verify(); // проверим запланированные вызовы 
        }
    }
}