﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using WebMvcMocking.NET.Command;
using WebMvcMocking.NET.Persistence;

namespace WebMvcMocking.NET.Tests.Command
{
    [TestClass]
    public class CreateMessageCommandTest
    {
        [TestMethod]
        public void TestExecute()
        {
            // инициализация исходных данных для теста
            const string expectedText = "Hello, world!";
            const int expectedUserSenderId = 111;
            const int expectedUserRecipientId = 222;
            const long expectedMessageId = 999L;
            Message actualMessage = null;
            var messagesRepositoryMock = new Mock<IMessagesRepository>();
            messagesRepositoryMock.Setup(r => r.CreateMessage(It.IsAny<Message>()))
                .Callback<Message>(m => actualMessage = m) // callback сохранит актуальный аргумент в локальную переменную
                .Returns(expectedMessageId);
            
            var command = new CreateMessageCommand(messagesRepositoryMock.Object);
            // вызываем тестируемый метод с исходными данными
            var actualMessageId = command.Execute(expectedText, expectedUserSenderId, expectedUserRecipientId);
            Assert.AreEqual(expectedMessageId, actualMessageId);
            // проверяем, правильно ли команда собрала объект для сохранения в БД 
            Assert.IsNotNull(actualMessage);
            Assert.AreEqual(expectedText, actualMessage.Text);
            Assert.AreEqual(expectedUserSenderId, actualMessage.UserSenderId);
            Assert.AreEqual(expectedUserRecipientId, actualMessage.UserRecipientId);
            Assert.IsNotNull(actualMessage.MessageDate);
        }
    }
}