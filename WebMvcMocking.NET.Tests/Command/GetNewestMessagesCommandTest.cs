﻿using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using WebMvcMocking.NET.Command;
using WebMvcMocking.NET.Persistence;

namespace WebMvcMocking.NET.Tests.Command
{
    [TestClass]
    public class GetNewestMessagesCommandTest
    {
        [TestMethod]
        public void TestExecute()
        {
            // создадим сообщения, которые должен вернуть messagesViewMock
            const int userId = 1;
            var message1 = new Message {Text = "text 1", Id = 1L, UserSenderId = 111, UserRecipientId = userId};
            var message2 = new Message {Text = "text 2", Id = 2L, UserSenderId = 333, UserRecipientId = userId};
            // настраиваем mock для метода messagesView.getMessages(): 
            // если входной параметр равен userId, то вернуть список наших сообщений
            var messagesViewMock = new Mock<IMessagesView>();
            messagesViewMock.Setup(v => v.GetMessages(userId)) // условие срабатывания мока
                .Returns(new [] {message1, message2}) // что вернуть
                .Verifiable(); // включить проверяемость этого вызова
            var userRepositoryMock = new Mock<IUserRepository>();

            // вызываем тестируемый метод, внутри происходит обращение к messagesViewMock и userRepositoryMock
            var command = new GetNewestMessagesCommand(userRepositoryMock.Object, messagesViewMock.Object);
            var actualMessages = command.Execute(userId).ToArray();
            Assert.AreEqual(2, actualMessages.Count());
            Assert.AreEqual(message1.Text, actualMessages[0].Text);
            Assert.AreEqual(message1.UserSenderId, actualMessages[0].UserSenderId);
            Assert.AreEqual(message1.UserRecipientId, actualMessages[0].UserRecipientId);
            Assert.AreEqual(message2.Text, actualMessages[1].Text);
            Assert.AreEqual(message2.UserSenderId, actualMessages[1].UserSenderId);
            Assert.AreEqual(message2.UserRecipientId, actualMessages[1].UserRecipientId);
            // проверяем обращения к messagesView
            messagesViewMock.Verify();
            // проверяем, что команда вызвала у userRepository нужный метод с правильными аргументами
            userRepositoryMock.Verify(r => r.SetLastViewedMessageId(userId, message2.Id));
        }
    }
}