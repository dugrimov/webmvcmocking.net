﻿using System;
using System.Globalization;
using System.Web.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using UnityAutoMoq;
using WebMvcMocking.NET.Command;
using WebMvcMocking.NET.Controllers;
using WebMvcMocking.NET.Models;

namespace WebMvcMocking.NET.Tests.Controllers
{
    [TestClass]
    public class MessageControllerTest
    {
        private Mock<ICreateMessageCommand> createMessageCommandMock;
        private Mock<IGetNewestMessagesCommand> getNewestMessagesCommandMock;
        private MessageController controller;

        [TestInitialize]
        public void Init()
        {
            // контейнер автоматически проинициализирует зависимости моками
            var container = new UnityAutoMoqContainer();
            // получаем инициализированный контроллер
            controller = container.Resolve<MessageController>();
            // получаем моки, которыми инициализирован контроллер
            createMessageCommandMock = container.GetMock<ICreateMessageCommand>();
            getNewestMessagesCommandMock = container.GetMock<IGetNewestMessagesCommand>();
        }

        [TestMethod]
        public void TestCreateMessage()
        {
            // инициализация исходных данных
            var message = new MessageModel {Text = "Привет!", UserSenderId = 1, UserRecipientId = 2};
            const long messageId = 999L;
            // настройка мока команды createMessageCommand
            createMessageCommandMock.Setup(c => c.Execute(It.IsAny<string>(), It.IsAny<Int32>(), It.IsAny<Int32>()))
                                    .Returns(messageId)
                                    .Verifiable();
            // вызов метода контроллера с приведением типа результата
            var result = controller.CreateMessage(message) as ContentResult;
            // проверка возвращенного результата
            Assert.IsNotNull(result);
            Assert.AreEqual(messageId.ToString(CultureInfo.InvariantCulture), result.Content);
            // проверяем, что контроллер вызвал команду с правильными аргументами
            createMessageCommandMock.Verify(c => c.Execute(message.Text, message.UserSenderId, message.UserRecipientId));
        }

        [TestMethod]
        public void TestNewMessages()
        {
            // инициализация исходных данных
            const int userId = 111;
            var message1 = new MessageModel {Text = "текст 1", UserRecipientId = userId, UserSenderId = 222};
            var message2 = new MessageModel {Text = "текст 2", UserRecipientId = userId, UserSenderId = 333};
            // настройка мока команды getNewestMessagesCommand
            getNewestMessagesCommandMock.Setup(c => c.Execute(userId))
                .Returns(new[] {message1, message2})
                .Verifiable();
            // вызов метода контроллера с приведением типа
            var result = controller.NewMessages(userId) as JsonResult;
            Assert.IsNotNull(result);
            // актуальные сообщения получаем приведением типа
            var actualMessages = result.Data as MessageModel[];
            // проверка возвращенного контента
            Assert.AreEqual(2, actualMessages.Length);
            Assert.AreEqual(message1.Text, actualMessages[0].Text);
            Assert.AreEqual(message1.UserSenderId, actualMessages[0].UserSenderId);
            Assert.AreEqual(message1.UserRecipientId, actualMessages[0].UserRecipientId);
            Assert.AreEqual(message2.Text, actualMessages[1].Text);
            Assert.AreEqual(message2.UserSenderId, actualMessages[1].UserSenderId);
            Assert.AreEqual(message2.UserRecipientId, actualMessages[1].UserRecipientId);
            // проверяем, что контроллер вызвал команду с правильными аргументами
            getNewestMessagesCommandMock.Verify();
        }
    }
}