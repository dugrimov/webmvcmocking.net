﻿using System;
using WebMvcMocking.NET.Models;

namespace WebMvcMocking.NET.Command
{
    public interface IGetMessageCommand
    {
        MessageModel Execute(Int64 messageId);
    }

    public class GetMessageCommand : IGetMessageCommand
    {
        public MessageModel Execute(Int64 messageId)
        {
            throw new NotImplementedException();
        }
    }
}