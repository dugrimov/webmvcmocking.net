﻿using System;

namespace WebMvcMocking.NET.Persistence
{
    public class Book
    {
        public Int32 Id { get; set; }
        public string Title { get; set; }
        public string AuthorName { get; set; } 
    }
}