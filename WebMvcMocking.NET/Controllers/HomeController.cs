﻿using System.Web.Mvc;
using WebMvcMocking.NET.Models;

namespace WebMvcMocking.NET.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View(new IndexModel { Foo = "Bar"});
        }
    }
}
