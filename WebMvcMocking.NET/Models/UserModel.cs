﻿using System;

namespace WebMvcMocking.NET.Models
{
    public class UserModel
    {
        public Int32 Id { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
    }
}